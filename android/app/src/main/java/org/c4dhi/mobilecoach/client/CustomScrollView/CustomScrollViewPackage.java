package org.c4dhi.mobilecoach.client.CustomScrollView;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class CustomScrollViewPackage implements ReactPackage {

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        //return Collections.emptyList();
        return Arrays.<ViewManager>asList(
                new ReactCustomScrollViewManager(),
                new ReactHorizontalScrollContainerViewManager(),
                new ReactHorizontalScrollViewManager()
        );
    }
}