package org.c4dhi.mobilecoach.client;

import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.preference.PreferenceManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Config {

    public static final int SENSOR_DELAY = SensorManager.SENSOR_DELAY_FASTEST;
    public static int[] sensorList = new int[]{
            Sensor.TYPE_LIGHT
    };
    public static final String SENSOR_FILE_EXTENSION = ".csv";
    public static boolean hasStartedSelfReport = false;
    public static boolean DymandFGServiceRunning = false;
    public static boolean onBoardingDone = false;
    public static int periodicLogsInMin = 30;
    public static int pendingIntentFlag = PendingIntent.FLAG_CANCEL_CURRENT;

    public static int webViewCloseTimerCount = 0;
    private static SimpleDateFormat df2 = new SimpleDateFormat("MM-dd-yyyy HH_mm_ss");

    public static String subjectID = "default";

    public static String getDateNowForFilename(){

        Calendar rightNow = Calendar.getInstance(); //get calendar instance
        return df2.format(rightNow.getTime());
    }

    public static void saveSubjectID(Context context){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("subjectID", subjectID);
        editor.apply();
    }

}