// Copyright 2004-present Facebook. All Rights Reserved.

package org.c4dhi.mobilecoach.client.CustomScrollView;

import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.views.scroll.ReactHorizontalScrollContainerView;

/** View manager for {@link com.facebook.react.views.scroll.ReactHorizontalScrollContainerView} components. */
@ReactModule(name = ReactHorizontalScrollContainerViewManager.REACT_CLASS)
public class ReactHorizontalScrollContainerViewManager
    extends ViewGroupManager<com.facebook.react.views.scroll.ReactHorizontalScrollContainerView> {

  protected static final String REACT_CLASS = "AndroidCustomHorizontalScrollContentView";

  public ReactHorizontalScrollContainerViewManager() {}

  @Override
  public String getName() {
    return REACT_CLASS;
  }

  @Override
  public com.facebook.react.views.scroll.ReactHorizontalScrollContainerView createViewInstance(ThemedReactContext context) {
    return new ReactHorizontalScrollContainerView(context);
  }
}
