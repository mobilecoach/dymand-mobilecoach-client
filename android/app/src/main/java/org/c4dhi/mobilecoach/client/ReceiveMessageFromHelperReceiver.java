package org.c4dhi.mobilecoach.client;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

public class ReceiveMessageFromHelperReceiver extends BroadcastReceiver {
  private String LOG_TAG = "Logs: ReceiveMessageFromHelperReceiver";

  public boolean isExternalStorageWritable() {
    String state = Environment.getExternalStorageState();
    if (Environment.MEDIA_MOUNTED.equals(state)) {
      return true;
    }
    return false;
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    Log.d(LOG_TAG, "Message from helper received");

    if(!isExternalStorageWritable()) {
      Toast.makeText(context, "External storage not available", Toast.LENGTH_SHORT);
      Log.e(LOG_TAG, "External storage not available");
      return;
    }

    File file = new File(context.getExternalFilesDir(null),"Dymand_BroadCastHelperLog");
    if(!file.exists()){
      file.mkdir();
    }
    try{
      File timeStamps = new File(file, "timestamps");
      FileWriter writer = new FileWriter(timeStamps, true);
      Timestamp ts=new Timestamp(System.currentTimeMillis());
      Date date=new Date(ts.getTime());
      writer.append(date+"\n");
      writer.flush();
      writer.close();

    }catch (Exception e){
      e.printStackTrace();
    }
  }
}
